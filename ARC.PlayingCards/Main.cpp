
// Playing Cards
// Alexander Rosas-Cortez
// Devon Was Here

#include <iostream>
#include <conio.h>

using namespace std;

//function prototypes
//void PrintCard(Card card);
//Card HighCard(Card card1, Card card2);

enum class Ranks
{
	Two = 2,
	Three, 
	Four , 
	Five, 
	Six ,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace

};

enum class Suits
{
	Clubs, 
	Diamonds,
	Hearts,
	Spades
};


struct Card
{
	Ranks Rank;
	Suits Suit;


};

void PrintCard(Card card) //Prints out the rank and suit of the card that is passed in as a parameter. Ex: The Queen of Diamond
{
	cout << "The ";
	switch (card.Rank)
	{
	case Ranks::Two: cout << "Two of "; break;
	case Ranks::Three: cout << "Three of "; break;
	case Ranks::Four: cout << "Four of "; break;
	case Ranks::Five: cout << "Five of "; break;
	case Ranks::Six: cout << "Six of "; break;
	case Ranks::Seven: cout << "Seven of "; break;
	case Ranks::Eight: cout << "Eight of "; break;
	case Ranks::Nine: cout << "Nine of "; break;
	case Ranks::Ten: cout << "Ten of "; break;
	case Ranks::Jack: cout << "Jack of "; break;
	case Ranks::Queen: cout << "Queen of "; break;
	case Ranks::King: cout << "King of "; break;
	case Ranks::Ace: cout << "Ace of "; break;
	}
	switch (card.Suit)
	{
	case Suits::Clubs: cout << "Clubs"; break;
	case Suits::Diamonds: cout << "Diamonds"; break;
	case Suits::Hearts: cout << "Hearts"; break;
	case Suits::Spades: cout << "Spades"; break;
	}
	cout << "\n";
}

Card HighCard(Card card1, Card card2) // Determines which of the two supplied cards has the higher rank.
{										//This should return a card.It should NOT print anything to the console.
	if (card1.Rank > card2.Rank)
	{
		return card1;
	}
	else
	{
		return card2;
	}
}

int main()
{

	Card c1;
	c1.Rank = Ranks::Two;
	c1.Suit = Suits::Diamonds;

	PrintCard(c1);

	Card c2;
	c2.Rank = Ranks::Ace;
	c2.Suit = Suits::Hearts;

	PrintCard(c2);

	cout << "The high card is: ";
	PrintCard(HighCard(c1, c2));


	(void)_getch();
	return 0;
}


